/*
 * positApprox_cpp.h
 *
 *  Created on: Jan 28, 2021
 *      Author: Raul Murillo
 */

#ifndef INCLUDE_POSITAPPROX_CPP_H_
#define INCLUDE_POSITAPPROX_CPP_H_

#include "positApprox.h"
#include "softposit_cpp.h"

//lam
inline posit8 lam(posit8 a, posit8 b){
	posit8 ans;
	ans.value = castUI( p8_lam(castP8(a.value), castP8(b.value)) );
	return ans;
}

inline posit16 lam(posit16 a, posit16 b){
	posit16 ans;
	ans.value = castUI( p16_lam(castP16(a.value), castP16(b.value) ) );
	return ans;
}

inline posit32 lam(posit32 a, posit32 b){
	posit32 ans;
	ans.value = castUI( p32_lam(castP32(a.value), castP32(b.value) ) );
	return ans;
}

#endif /* INCLUDE_POSITAPPROX_CPP_H_ */
