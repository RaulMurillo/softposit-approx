/*
 * positApprox.h
 *
 *  Created on: Jan 28, 2021
 *      Author: Raul Murillo
 */


#include "softposit.h"

#ifdef SOFTPOSIT_QUAD
#include <quadmath.h>
#endif


#ifndef INCLUDE_SOFTPOSIT_APPROX_H_
#define INCLUDE_SOFTPOSIT_APPROX_H_


#ifdef __cplusplus
extern "C"{
#endif

posit32_t p32_lam (posit32_t, posit32_t);

posit16_t p16_lam (posit16_t, posit16_t);

posit8_t p8_lam (posit8_t, posit8_t);



#ifdef __cplusplus
}
#endif

#endif

