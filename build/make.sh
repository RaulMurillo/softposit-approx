#!/bin/bash

export SP_CURDIR=`pwd`
if [[ -z "${SOFTPOSIT}" ]]; then
    cd ../..
    echo "Downloading and installing SoftPosit at: " `pwd`

    FILE=SoftPosit-master.tar.gz
    if [ -x "$(command -v wget)" ]; then
        wget https://gitlab.com/RaulMurillo/SoftPosit/-/archive/master/$FILE
    elif [ -x "$(command -v curl)" ]; then
        curl -O https://gitlab.com/RaulMurillo/SoftPosit/-/archive/master/$FILE
    else
        echo "Neither wget nor curl is available to download SoftPosit."
        exit 1
    fi
    if [ -f "$FILE" ] && [ -x "$(command -v tar)" ]; then
        tar xzvf $FILE
        rm $FILE
    else
        echo "Unable to extract $FILE"
        exit 1
    fi

    cd SoftPosit-master
    export SOFTPOSIT=`pwd`
    cd build/Linux-x86_64-GCC
    make clean
    make -j6 all
fi
echo $SOFTPOSIT
cd $SP_CURDIR
make clean
make all SOFTPOSIT=`echo $SOFTPOSIT`


