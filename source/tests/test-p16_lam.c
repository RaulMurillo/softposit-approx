
/*============================================================================

This C source file is part of the SoftPosit Approximate Package
by Raul Murillo.

Copyright 2021 Raul Murillo.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions, and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions, and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of the University nor the names of its contributors may
    be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS "AS IS", AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE
DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/

#include "softposit.h"
#include "softposit_types.h"
#include "positApprox.h"

#include <stdio.h>
#include <math.h>

void displayBinary(uint64_t * s, int size);

int main (int argc, char *argv[]){

    // TODO

    // posit16_t pA, pB, pZ;

    // int i=0, count=0;
    // uint16_t uiZ;
    // double dA, dZ;
    // bool test = 0;

    // // Section for all exception values
    // for (i=32769; i<36691; i++)	{		// minpos set

    // 	pA.v = i;
    // 	pZ = p16_exp(pA);
    // 	pB.v = 1;
    // 	test = p16_eq(pZ, pB);

    //     if (!test) {
    //     	printf("Error at posit %d  ", i);
    //     	displayBinary((uint64_t*)&castUI(pZ), 16);
    //     	printf("  ");
    //     	displayBinary((uint64_t*)&castUI(pB), 16);
    //     	printf("   %f", convertP16ToDouble(pZ));
    //     	printf("   %f", dZ);
    //     	printf("\n");
    //     	count++;
    //     }
    // }
    // for (i=28846; i<32768; i++)	{		// maxpos set

    // 	pA.v = i;
    // 	pZ = p16_exp(pA);
    // 	pB.v = 32767;
    // 	test = p16_eq(pZ, pB);

    //     if (!test) {
    //     	printf("Error at posit %d  ", i);
    //     	displayBinary((uint64_t*)&castUI(pZ), 16);
    //     	printf("  ");
    //     	displayBinary((uint64_t*)&castUI(pB), 16);
    //     	printf("   %f", convertP16ToDouble(pZ));
    //     	printf("   %f", dZ);
    //     	printf("\n");
    //     	count++;
    //     }
    // }
    // for (i=0; i<192; i++)	{		// positive values that rounds to 1

    // 	pA.v = i;
    // 	pZ = p16_exp(pA);
    // 	pB.v = 16384;
    // 	test = p16_eq(pZ, pB);

    //     if (!test) {
    //     	printf("Error at posit %d  ", i);
    //     	displayBinary((uint64_t*)&castUI(pZ), 16);
    //     	printf("  ");
    //     	displayBinary((uint64_t*)&castUI(pB), 16);
    //     	printf("   %f", convertP16ToDouble(pZ));
    //     	printf("   %f", dZ);
    //     	printf("\n");
    //     	count++;
    //     }
    // }
    // for (i=65408; i<65536; i++)	{		// negative values that rounds to 1

    // 	pA.v = i;
    // 	pZ = p16_exp(pA);
    // 	pB.v = 16384;
    // 	test = p16_eq(pZ, pB);

    //     if (!test) {
    //     	printf("Error at posit %d  ", i);
    //     	displayBinary((uint64_t*)&castUI(pZ), 16);
    //     	printf("  ");
    //     	displayBinary((uint64_t*)&castUI(pB), 16);
    //     	printf("   %f", convertP16ToDouble(pZ));
    //     	printf("   %f", dZ);
    //     	printf("\n");
    //     	count++;
    //     }
    // }

    // // Single NaR case
    // pA.v = 32768;
    // pZ = p16_exp(pA);
    // pB.v = 32768;
    // test = p16_eq(pZ, pB);
    // if (!test) {
    // 	printf("Error at posit %d  ", i);
    // 	displayBinary((uint64_t*)&castUI(pZ), 16);
    // 	printf("  ");
    // 	displayBinary((uint64_t*)&castUI(pB), 16);
    // 	printf("   %f", convertP16ToDouble(pZ));
    // 	printf("   %f", dZ);
    // 	printf("\n");
    // 	count++;
    // }

    // // Section for all negative posits
    // for (i=36691; i<65408; i++)		// IGNORE exception set for minpos
    // {
    // 	pA.v = i;			// posit number
    // 	pZ = p16_exp(pA);
    // 	dA = convertP16ToDouble(pA);	// posit value
    //     dZ = exp(dA);			// double value of exp(posit value)
    //     pB = convertDoubleToP16(dZ);	// answer to posit
    //     test = p16_eq(pZ, pB);		// is exp(posit value) the same as
    //     				// posit from double value of exp(posit value) ?
    //     if (!test) {
    //     	printf("Error at posit %d  ", i);
    //     	displayBinary((uint64_t*)&castUI(pZ), 16);
    //     	printf("  ");
    //     	displayBinary((uint64_t*)&castUI(pB), 16);
    //     	printf("   %f", convertP16ToDouble(pZ));
    //     	printf("   %f", dZ);
    //     	printf("\n");
    //     	count++;
    //     }
    // }

    // // Section for all positive posits
    // for (i=192; i<28846; i++)		// IGNORE exception set for maxpos
    // {
    // 	pA.v = i;			// posit number
    // 	pZ = p16_exp(pA);
    // 	dA = convertP16ToDouble(pA);	// posit value
    //     dZ = exp(dA);			// double value of exp(posit value)
    //     pB = convertDoubleToP16(dZ);	// answer to posit
    //     test = p16_eq(pZ, pB);		// is exp(posit value) the same as
    //     				// posit from double value of exp(posit value) ?
    //     if (!test) {
    //     	printf("Error at posit %d  ", i);
    //     	displayBinary((uint64_t*)&castUI(pZ), 16);
    //     	printf("  ");
    //     	displayBinary((uint64_t*)&castUI(pB), 16);
    //     	printf("   %f", convertP16ToDouble(pZ));
    //     	printf("   %f", dZ);
    //     	printf("\n");
    //     	count++;
    //     }
    // }

    // if (count) {
    // 	printf("\nTest failed for %d posits!\n", count);
    // 	return 1;
    // } else {
    // 	printf("Test passed for all lam(posit16) !\n");
    // 	return 0;
    // }

}

void displayBinary(uint64_t * s, int size) {
	int i;
	uint64_t number = *s;
	int bitSize = size -1;
	for(i = 0; i < size; ++i) {
		if(i%8 == 0)
			putchar(' ');
		printf("%lu", (number >> (bitSize-i))&1);
	}

}
