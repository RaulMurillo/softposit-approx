#include "positApprox.h"
#include <stdio.h>
#include <omp.h>

int main (int argc, char *argv[]){

    // TODO

//     posit16_t pA, pZ;
//     double time;
//     int i=0, numthreads;
//     unsigned long j=0, end;

// #pragma omp parallel
// #pragma omp master
//     numthreads = omp_get_num_threads();

//     end = numthreads*24000;

//     printf("Benchmarking exponential posit operations using %d threads.\n", numthreads);
//     time = omp_get_wtime();

// #pragma omp parallel for private(i,pA,pZ)
//     for (j=0; j<end; j++) {

// 		for (i=36691; i<65408; i++)			// 28717 iterations
// 		{
// 			pA.v = i;
// 			pZ = p16_exp(pA);
// 		}
// 		if (numthreads > 65535) printf("Dummy line");	// Prevent compiler optimising out loop
// 		for (i=192; i<28846; i++)			// 28654 iterations
// 		{
// 			pA.v = i;
// 			pZ = p16_exp(pA);
// 		}
// 		if (numthreads > 65535) printf("Dummy line");	// Prevent compiler optimising out loop

//     }
//     time = omp_get_wtime() - time;

//     printf("Done %lu exponential posit operations in %f seconds, using %d threads.\n", end * 57371, time, numthreads);
//     printf("Speed is %.0f pop/s.\n", (double) (end * 57371)/time);

    return 0;

}

