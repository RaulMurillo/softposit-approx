# SoftPosit-Approx

Contains approximate functions for posits.

Inspired in the [SoftPosit-Math](https://gitlab.com/cerlane/softposit-math) extension module.

Math approximate functions added for posits:
*  logarithmic-approximate multiplication/lam (8, 16, 32 bits)

# To build

## Linux and OS-X

```
cd softposit-approx/build
./make.sh
```

This will automatically download and build SoftPosit, and compile SoftPosit-Approx.

To build the combined single library containing SoftPosit, use:

```
make single-lib
```

The library can be tested for correctness:

```
make check
```

The micro-benchmarks (OpenMP required) can be built with:

```
make benchmarks
```

## Windows (not tested)

(1) Download [SoftPosit](https://gitlab.com/RaulMurillo/SoftPosit)  and install it. 

(2) Download SoftPosit-Approx.

(3) To install: Set environment variable "SOFTPOSIT"

```
set SOFTPOSIT={DIR}\SoftPosit
```

(4) To install: Compile

```
cd {DIR}\softposit-approx\build
make all

```

# To use

For example, if you have a file "main.c" that uses both SoftPosit-Approx and SoftPosit, you can compile your code as such:

```
gcc -lm  -o main main.c {DIR}/softposit-approx/build/softposit_approx.a \
      {DIR}/SoftPosit-master/build/Linux-x86_64-GCC/softposit.a -O2 \
    -I{DIR}/SoftPosit-master/build/Linux-x86_64-GCC \
    -I{DIR}/SoftPosit-master/source/include \
    -I{DIR}/softposit-approx/include 
```

or with the single library:

```
gcc -lm  -o main main.c {DIR}/softposit-approx/build/softposit_approx.a -O2 \
    -I{DIR}/SoftPosit-master/build/Linux-x86_64-GCC \
    -I{DIR}/SoftPosit-master/source/include \
    -I{DIR}/softposit-approx/include
```

The micro-benchmarks can be run with the desired number of OpenMP threads:

```
OMP_NUM_THREADS=4 ./benchmark-p16_lam.exe
```
